// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

/*
 The App.init() function will be invoked on page load and it will set up handlers for form submission and for
 receiving messages over the channel.

 ES6 syntax:
 The first line imports the Socket object from web/static/vendor/phoenix.js.
 The static init() adds the function to the App object so that we can invoke it as App.init().
 The $( () => ... ) line uses the arrow syntax for functions and is equivalent to $(function() { App.init() }).
 The jQuery function $() gets called when the page is loaded.
*/



// Import the Socket object from phoenix.js (see: deps/phoenix/web/static/js/phoenix.js)
import {Socket} from "phoenix"

class App {

  /*
    Invoked on page load.
    Set up handlers for form submission and for receiving messages over the channel.
    Submit the fields when the user hits enter from the #message text field (see: web/templates/page/index.html.eex).
  */

  // Adds the function to the App object so that it can be invoked as App.init()
  static init() {
    // (see: web/templates/page/index.html.eex)
    var username = $("#username")
    var msgBody  = $("#message")

    // Connect to the channel on the page load event
    let socket = new Socket("/socket")
    socket.connect()
    socket.onClose( e => console.log("Closed connection") )

    var channel = socket.channel("rooms:lobby", {})
    channel.join()
      .receive( "error", () => console.log("Connection error") )
      .receive( "ok",    () => console.log("Connected") )

    /*
     Clear all the keypress event handlers and then add a new one which pushes a message submission to the server
     when the enter key is pressed (key code = 13).
    */
    msgBody.off("keypress").on("keypress", e => {
      if (e.keyCode == 13) {
        //console.log(`[${username.val()}] ${msgBody.val()}`)   // ES6 string interpolation
        channel.push("new:message", {
          user: username.val(),
          body: msgBody.val()
        })
        msgBody.val("")
      }
    })

    // handle broadcast messages from the server
    //channel.on( "new:message", msg => console.log(msg.body) )
    channel.on( "new:message", msg => this.renderMessage(msg) )
  }

  // Append messages to the #messages div on the web page
  static renderMessage(message) {
    var messages = $("#messages")
    var user = this.sanitize(message.user || "New User")
    var body = this.sanitize(message.body)
    messages.append(`<p><b>[${user}]</b>: ${body}</p>`)
  }

  // prevent malicious javascript as part of a message
  static sanitize(str) { return $("<div/>").text(str).html() }
}

/*
  called when the page is loaded
  equivilent to $(function() { App.init() })
*/
$( () => App.init() )

export default App